#include <unistd.h>
#include <cstdio>
#include <termios.h>
#include <algorithm>

#include "DirectInput.hpp"

/**
    Source:
    https://stackoverflow.com/questions/421860/capture-characters-from-standard-input-without-waiting-for-enter-to-be-pressed
    But it reads direct input without enter. Black magic function. No idea how it actually works.
*/
char getch()
{
    char buf = 0;
    struct termios old = {0};
    if (tcgetattr(0, &old) < 0)
        perror("tcsetattr()");
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0)
        perror("tcsetattr ICANON");
    if (read(0, &buf, 1) < 0)
        perror("read()");
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0)
        perror("tcsetattr ~ICANON");
    return (buf);
}

char input;
void DirectInput::UpdateInput()
{
    input = getch();
}

void DirectInput::Dispatch()
{
    if (input != '\0')
    {
        for (IInputObserver *observer : observers)
        {
            observer->HandleInput(input);
        }
    }
    input = '\0';
}

void DirectInput::Subscripe(IInputObserver *observer)
{
    observers.push_back(observer);
}

void DirectInput::Unsubscripe(IInputObserver *observer)
{
    auto iterator = find(observers.begin(), observers.end(), observer);

    if (iterator != observers.end())
    {
        observers.erase(iterator);
    }
}
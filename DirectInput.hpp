#pragma once
#include <vector>
using namespace std;

/**
    Interface for user keyboard inputs. Required to be implemented to receive Events from DirectInput
*/
class IInputObserver
{
  public:
    /**
        Virtual Input event handler. Dispatched when new input is received from user
        @param 
    */
    virtual void HandleInput(char curInput) = 0;
};

class DirectInput
{
    vector<IInputObserver *> observers;

  public:
    /**
        Static method for updating current input char. Does not do even dispatch yet. 
        Call Dispatch() to actually invoke the events
    */
    static void UpdateInput();

    /**
        Dispatches events to all observers. Uses current user char value
    */
    void Dispatch();

    /**
        Subscripe / register to receive keyboard events
        @param observer to subscripe
    */
    void Subscripe(IInputObserver *observer);

    /**
        Unsubscripe to not receive events anymore
        @param observer to unsubscripe
    */
    void Unsubscripe(IInputObserver *observer);
};
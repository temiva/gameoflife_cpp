#include "ConsoleDisplay.hpp"
#include <iostream>
#include <algorithm>

CursorPosition ConsoleDisplay::GetCursor()
{
    return CursorPosition{.x = m_cursorX, .y = m_cursorY};
}

void ConsoleDisplay::MoveCursor(int dirx, int diry)
{
    m_cursorX += dirx;
    m_cursorY += diry;

    if (m_cursorX < 0)
        m_cursorX = 0;

    if (m_cursorY < 0)
        m_cursorY = 0;
}

void ConsoleDisplay::ClampCursorPosition(int width, int height)
{
    if (m_cursorX > width - 1)
        m_cursorX = width - 1;
    if (m_cursorY > height - 1)
        m_cursorY = height - 1;
}

void ConsoleDisplay::SetGrid(const Grid &grid)
{
    //Make sure cursor is withing grid bounds and therefore visible
    ClampCursorPosition(grid.GetWidth(), grid.GetHeight());

    m_gridStr = "";

    cout << endl;
    auto gridArr = grid.GetCells();
    for (size_t y = 0; y < grid.GetHeight(); y++)
    {
        for (size_t x = 0; x < grid.GetWidth(); x++)
        {
            if (x == m_cursorX && y == m_cursorY)
                m_gridStr += (gridArr[x][y] ? "[O]" : "[.]");
            else
                m_gridStr += (gridArr[x][y] ? " O " : " . ");
        }
        m_gridStr += "\n";
    }
}

void ConsoleDisplay::Print()
{
    if (!m_gridStr.empty())
        cout << m_gridStr;

    if (!m_helpStr.empty())
        cout << m_helpStr << endl;
}

void ConsoleDisplay::SetHelpText(string helpStr)
{
    m_helpStr = helpStr;
}

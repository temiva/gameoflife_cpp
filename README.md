**PREREQUISITES**


* Project was compiled and tested on Ubuntu 18. It *should* work on other linux systems but I couldn't test it in practice.  
* Project does not compile on Windows. 
* There are potential issues regarding <termios.h>. Stack overflow tells me that it could be blocked on some systems. 



**COMPILING**

Make sure you have gcc and make installed. If you don't then install them:
`$ sudo apt install gcc`
`$ sudo apt install make`

Build and run project:
`$ make && ./gameoflife`




**USAGE**

Everything is controlled via keyboard

Move cursor                      [W][A][S][D]

Resize current grid              [J][L][I][K]

Add/Remove cell under cursor     [M]

Simulate one step                [U]

Pause/Begin simulation           [P]

Random fill cells                [R]

Exit                          2x [Ctrl + C] :-) 




**DESIGN DISCUSSION**

Main handles main and input thread. Input thread is constantly polling user input and sets it into a variable. Main thread dispatches input events and updates the game loop.

There is a simple observer pattern design on DirectInput which allows objects implementing IInputObserver interface to register to input events. Inputs are simple chars.

Dependency injection was used on objects where it felt approriate to make testing potentially a bit more easy. 

GameLoop is the core orchestrator. It keeps track of Grid and Display objects. It also is registered to listen to any input events coming from the input thread.

Grid handles each game simulation step. It keeps track of current and previous grid state. Cells are simple bools. Two grid states are needed so reading and writing can be done properly. When a step is done, previous and current is simply swapped and reused. 
Grid could perhaps have some sort of logic for having different rules. There could for example be a array of rule functions that are run all on each update step. Having it like it currently is was a decission to choose simplicity over flexibility. 

Display tries to be "stupid" / mostly stateless and has its state fed in externally. Then it just prints its current state on demand. It could potentially be replaced with a proper graphical user interface rather easily. 
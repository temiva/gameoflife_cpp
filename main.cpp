#include <memory>
#include <thread>
#include "DirectInput.hpp"
#include "GameLoop.hpp"

/**
    Updates current user input by calling DirectInput's Update. This is supposed to 
    be run in a thread
    @param sleep time between updates.
*/
void inputLoop(unsigned int period_msecs)
{
    const auto interval = std::chrono::milliseconds(period_msecs);
    int cnt = 0;
    while (true)
    {
        DirectInput::UpdateInput();
        std::this_thread::sleep_for(interval);
    }
}

int updateCounter;
int main()
{

    //Create separate thread for input reading so user doesn't have to confirm inputs
    unique_ptr<DirectInput> input = make_unique<DirectInput>();
    thread(inputLoop, 10).detach();

    unique_ptr<GameLoop> gameLoop = make_unique<GameLoop>(*input.get());

    //Update game and input loops
    while (true)
    {
        input->Dispatch();
        this_thread::sleep_for(chrono::milliseconds(10));

        //Throttle updatespeed so we can see what is happening
        updateCounter++;
        if (updateCounter > 30)
        {
            gameLoop->Update();
            updateCounter = 0;
        }
    }
    return 0;
}

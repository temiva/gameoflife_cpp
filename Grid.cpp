#include "Grid.hpp"

Grid::Grid(int width, int height)
{
    m_width = width;
    m_height = height;
    auto cells = m_width * m_height;

    m_cells = vector<vector<bool>>(width, vector<bool>(height));
    m_cellsPrevious = vector<vector<bool>>(width, vector<bool>(height));
}

void Grid::Update()
{
    Swap();
    for (int y = 0; y < GetHeight(); y++)
    {
        for (int x = 0; x < GetWidth(); x++)
        {
            bool alive = IsAlive(x, y);
            auto numNeighbours = NumAliveNeighbours(x, y);

            //underpopulation: If alive and less than 2 alive neighbours -> kill
            //overpopulation: If alive and has 4 alive neighbours -> kill
            if (alive && (numNeighbours <= 1 || numNeighbours >= 3))
                SetCell(x, y, false, false);
            //If dead and 3 alive neighbours -> new cell is born
            else if (numNeighbours == 3)
                SetCell(x, y, true, false);
        }
    }
}

void Grid::Swap()
{
    m_cellsPrevious = m_cells;
}

int Grid::NumAliveNeighbours(const int x, const int y)
{
    if (IsWithinBounds(x, y))
    {
        //Sum all alive neighbour cells
        int num = 0;
        for (int ix = -1; ix <= 1; ix++)
        {
            for (int iy = -1; iy <= 1; iy++)
            {
                //Skip self
                if (ix == 0 && iy == 0)
                    continue;

                if (IsAlive(x + ix, y + iy))
                    num++;
            }
        }
        return num;
    }
    return 0;
}

bool Grid::IsWithinBounds(int x, int y)
{
    return x < m_width && y < m_height && x >= 0 && y >= 0;
}

bool Grid::IsAlive(int x, int y)
{
    return IsWithinBounds(x, y) && m_cellsPrevious[x][y];
}

bool Grid::ToggleCell(int x, int y)
{
    if (IsWithinBounds(x, y))
    {
        SetCell(x, y, !m_cells[x][y], true);
    }
}

bool Grid::SetCell(int x, int y, bool alive, bool overwritePrevious)
{
    if (IsWithinBounds(x, y))
    {
        m_cells[x][y] = alive;
        if (overwritePrevious)
            m_cellsPrevious[x][y] = alive;
        return true;
    }
    else
    {
        return false;
    }
}

const vector<vector<bool>> &Grid::GetCells() const
{
    return m_cells;
}

int Grid::GetWidth() const
{
    return m_width;
}

int Grid::GetHeight() const
{
    return m_height;
}
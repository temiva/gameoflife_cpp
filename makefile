all: main

main: main.o Grid.o ConsoleDisplay.o GameLoop.o DirectInput.o
	 g++ -o gameoflife main.o Grid.o ConsoleDisplay.o GameLoop.o DirectInput.o -pthread

main.o: main.cpp Grid.cpp ConsoleDisplay.cpp GameLoop.cpp DirectInput.cpp
	 g++ -c main.cpp Grid.cpp ConsoleDisplay.cpp GameLoop.cpp DirectInput.cpp -pthread

clean:
	 rm *.o main
#pragma once
#include <memory>
#include "Grid.hpp"
#include "ConsoleDisplay.hpp"
#include "DirectInput.hpp"

using namespace std;

class GameLoop : public IInputObserver
{
    bool m_autoUpdate = false;
    unique_ptr<Grid> m_grid;
    unique_ptr<ConsoleDisplay> m_display;

    /**
        Handles grid resizing event, rescales grid
        @param current user keyboard key 
    */
    void HandleResizeInput(char input);

    /**
        Handles cursor movement
        @param current user keyboard key 
    */
    void HandleCursorInput(char input);

    /**
        Handles gameplay events. Simulation pause and begin
        @param current user keyboard key 
    */
    void HandleLoopInput(char input);

    /**
        Updates current m_display values and prints state on console
    */
    void UpdateDisplay();

    /**
        Updates game grid and prints new state on screeen
        @param skip m_autoUpdate validation. true to print on screen and skip any validations 
    */
    void Update(bool forceUpdate);

    /**
        Randomizes current grid by toggling grid cells. 
    */
    void RandomizeGrid();

  public:
    /**
        Constructor 
        @param DirectInput reference to which this GameLoop will subscripe / register to.
    */
    GameLoop(DirectInput &inputHandler);

    /**
        Deleted default constructor to force DirectInput dependency 
    */
    GameLoop() = delete;

    /**
        Updates current game state. Will not do anything if private m_autoUpdate is set to false 
    */
    void Update();

    /**
        Implementation of DirectInput's IInputObserver interface.
        @param current user keyboard key 
    */
    virtual void HandleInput(char curInput) override;
};
#include <string>
#include "GameLoop.hpp"

GameLoop::GameLoop(DirectInput &inputHandler)
{
    //Subscripe to user input events
    inputHandler.Subscripe(this);

    //Create grid and display
    m_grid = make_unique<Grid>(10, 10);
    m_display = make_unique<ConsoleDisplay>();

    UpdateDisplay();
}

void GameLoop::UpdateDisplay()
{
    //Send current grid status to be displayed
    m_display->SetGrid(*m_grid.get());

    string helpStr = "Cursor:[A][D][W][S]  Add[M]  Resize:[J][L][I][K]  Update:[U] \n";
    helpStr += m_autoUpdate ? "STOP[P]" : "START[P]";
    helpStr += "  Random [R]";
    m_display->SetHelpText(helpStr);

    m_display->Print();
}

void GameLoop::Update()
{
    Update(false);
}

void GameLoop::Update(bool forceUpdate)
{
    if (m_autoUpdate || forceUpdate)
    {
        m_grid->Update();
        UpdateDisplay();
    }
}

void GameLoop::RandomizeGrid()
{
    //Random number of cells based on current grid size
    auto num = m_grid->GetWidth() * m_grid->GetHeight() / 3;
    for (size_t i = 0; i < num; i++)
    {
        int x = rand() % m_grid->GetWidth();
        int y = rand() % m_grid->GetHeight();
        m_grid->ToggleCell(x, y);
    }
}

void GameLoop::HandleLoopInput(char input)
{
    switch (input)
    {
    case 'u':
        Update(true);
        break;
    case 'p':
        m_autoUpdate = !m_autoUpdate;
        break;
    case 'r':
        RandomizeGrid();
        break;
    }
}

void GameLoop::HandleCursorInput(char input)
{
    int x = 0;
    int y = 0;

    switch (input)
    {
    case 'w':
        y--;
        break;
    case 's':
        y++;
        break;
    case 'a':
        x--;
        break;
    case 'd':
        x++;
        break;
    case 'm':
    {
        CursorPosition cursor = m_display->GetCursor();
        m_grid->ToggleCell(cursor.x, cursor.y);
    }
    break;
    }

    m_display->MoveCursor(x, y);
}

void GameLoop::HandleResizeInput(char input)
{
    //Hold curerent and new state to determine if rescaling is needed
    auto curW = m_grid->GetWidth();
    auto curH = m_grid->GetHeight();
    auto newW = curW;
    auto newH = curH;

    //Increment or decrement size based on input value
    switch (input)
    {
    case 'j':
        newW = newW - 1 > 0 ? newW - 1 : 1; //clamp out negative and 0 values
        break;
    case 'l':
        newW++;
        ;
        break;
    case 'k':
        newH = newH - 1 > 0 ? newH - 1 : 1; //clamp out negative and 0 values
        break;
    case 'i':
        newH++;
        break;
    }

    //Create a new grid if size has changed
    if (curW != newW || curH != newH)
        m_grid = make_unique<Grid>(newW, newH);
}

void GameLoop::HandleInput(char curInput)
{
    HandleResizeInput(curInput);
    HandleCursorInput(curInput);
    HandleLoopInput(curInput);
    UpdateDisplay();
}
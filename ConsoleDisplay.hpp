#pragma once
#include <vector>
#include <string>
#include "Grid.hpp"

using namespace std;
struct CursorPosition
{
    int x;
    int y;
};

class ConsoleDisplay
{
    string m_gridStr;
    string m_helpStr;
    int m_cursorX = 5;
    int m_cursorY = 5;

    /**
        Updates cursor's position to not exceed given width or height. 
        @param max cursor width.
        @param max cursor height.
    */
    void ClampCursorPosition(int width, int height);

  public:
    /**
        Sets grid to be printed. Does not print anything yet
        @param const grid reference
    */
    void SetGrid(const Grid &grid);

    /**
        Moves cursor by given x and y 
        @param x offset from current position
        @param y offset from current position
    */
    void MoveCursor(int dirx, int diry);

    /**
        Get current user cursor position
        @return current cursor position
    */
    CursorPosition GetCursor();

    /**
        Assign help text string to be shown when Print() is called. Will be shown below grid status
        Only sets state. Does not print anything yet
        @param string to be shown below grid
    */
    void SetHelpText(string helpStr);

    /**
        Outputs current state to console 
    */
    void Print();
};

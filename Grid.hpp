#pragma once
#include <vector>
using namespace std;

class Grid
{
    int m_width;
    int m_height;
    vector<vector<bool>> m_cellsPrevious;
    vector<vector<bool>> m_cells;

    /**
        Checks wether given coordinate is inside grid bounds. aka. between 0 and m_width
        and 0 and m_height 
        @param grid x coordinate
        @param grid y coordinate
        @return true if coordinate is within grid bounds
    */
    bool IsWithinBounds(int x, int y);

    /**
        Get given coordinate's cell status. aka alive or not
        @param grid x coordinate
        @param grid y coordinate
        @return true if coordinate has alive cell. false if its dead
    */
    bool IsAlive(int x, int y);

    /**
        How many alive direct neighbours there are around a given coordinate
        @param cell x coordinate
        @param cell y coordinate
        @return number of direct neighbours. Does not count given coordinate itself
        so can return up to 8 alive
    */
    int NumAliveNeighbours(int x, int y);

    /**
        Swaps grids from m_cells to m_cellsPrevious. 
    */
    void Swap();

    /**
        Sets given cell status to dead or alive
        @param cell x coordinate
        @param cell y coordinate
        @param alive state of the cell. false equals dead, true equals alive
        @param boolean to determine if the m_cellsPrevious state should be set aswell
    */
    bool SetCell(int x, int y, bool alive, bool overwritePrevious = true);

  public:
    /**
        Constructor
        @param grid width
        @param grid height
    */
    Grid(int width, int height);

    /**
        Force valid size by disabling default constructor
    */
    Grid() = delete;

    /**
        Simulates the grid one step forward
    */
    void Update();

    /**
        Toggles given coordinate's cell alive state. If it was dead then it will 
        be alive and vice versa.
        @param cell x coordinate.
        @param cell y coordinate.
    */
    bool ToggleCell(int x, int y);

    /**
        Get current grid of cells internal state
        @return const two dimensional vector of cell states. true equals alive cell. 
        false equals dead cell
    */
    const vector<vector<bool>> &GetCells() const;

    /**
        Get grid's width
        @return grid's width
    */
    int GetWidth() const;

    /**
        Get grid's height
        @return grid's height
    */
    int GetHeight() const;
};
